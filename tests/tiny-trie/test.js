const Trie = require("tiny-trie");
const PackedTrie = require("tiny-trie/lib/PackedTrie");
const fs = require("fs");

let words = [];

const rawDocuments = fs.readFileSync("./tests/_input/1.15M-documents.txt", "utf-8");
rawDocuments.split(/\r?\n/).forEach(line =>  {
    words.push(line.split("\t")[1]);
});
words.sort();

const trie = Trie.createSync(words);
const encodedTrie = trie.encode();
fs.writeFileSync("./tests/tiny-trie/encodedTrie.b64", encodedTrie);

/* console.log(PackedTrie.prototype(encodedTrie));
const smallDawg = new PackedTrie(encodedTrie);
console.log(smallDawg); */

//var result = trie.search("agio", {prefix: true});
//console.log(processResult(result));

/*

//const loadedTrie = PackedTrie.prototype(encodedTrie);

var result = trie.search("agio", {prefix: true});
console.log(processResult(result));

result = trie.search("***cru***", {wildcard: "*"});
console.log(result);

/*
const trie = new Trie();

words.forEach(word => trie.insert(word));

// test membership
trie.test('spit');
// -> true
trie.test('split');
// -> false
trie.search('spi', {prefix: true});
// -> ['spit', 'spits']

// finalize the trie, turning it into a DAWG
trie.freeze();

// encode the trie
let encoded = trie.encode();
// -> 'A4AAAAMEspiaotI0NmhqfPzcsQLwwrCCcBAQE'

// This string describes the DAWG in a concise binary format. This format can
// be interpreted by the `PackedTrie` class.
const smallDawg = new PackedTrie(encoded);

smallDawg.test('spit');
// -> true
smallDawg.test('split');
// -> false
smallDawg.search('spi', {prefix: true});
// -> ['spit', 'spits']
*/

const fs = require("fs");
const path = require("path");
const os = require("os");
const perfect = require("./perfect-hash-functions.js");

const dict = {};

const fileContents = fs.readFileSync(path.resolve(__dirname, "../_input/1.15M-documents.txt"), "utf8");
const fileLines = fileContents.split(os.EOL);

fileLines.forEach(fileLine => {
  const fileLineSplitted = fileLine.split("\t");
  const uuid = fileLineSplitted[0];
  const word = fileLineSplitted[1];

  dict[uuid] = word
});

const tables = perfect.create(dict);

console.log(tables);

Object.keys(dict).forEach(function(key) {
  //console.log('key: ' + key + ' value: ' +  perfect.lookup( tables[0], tables[1], key));
});
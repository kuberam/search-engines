const Fuse =require("./fuse.min.js");
const fs = require("fs");
const path = require("path");
const os = require("os");

const replacements = {"ă": "a", "â": "a", "î": "i","ș": "s", "ş": "s","ț": "t","ţ": "t","á": "a","é": "e","í": "i","ó": "o","ú": "u","ắ": "a","ấ": "a","î́": "i"};
const replacementRegexp = new RegExp("[" + Object.keys(replacements).join("|") + "]", 'g')

const fileContents = fs.readFileSync(path.resolve(__dirname, "../_input/30k-documents.txt"), "utf8");
const fileLines = fileContents.split(os.EOL);
const processedDocuments = fileLines.map(fileLine => {
    const fileLineSplitted = fileLine.split("\t");
    const word = fileLineSplitted[1];

    return {"s": word}
});

const options = { 
  useExtendedSearch: true,
  ignoreFieldNorm: true,
  keys: ["s"]
};

const fuseIndex = Fuse.createIndex(options.keys, processedDocuments);

const fuse = new Fuse(processedDocuments, options, fuseIndex);
console.time("search1")
const result = fuse.search("'ac 'b b$");
console.timeEnd("search1")

console.log(result);
//console.log(fuse.getIndex());


const rawIndex = JSON.stringify(fuseIndex.toJSON());
//console.log(rawIndex);
const indexModule = `
const index = '${rawIndex}';

export { index };
`;
fs.writeFileSync(path.resolve(__dirname, "index/30k-documents.json"), indexModule);
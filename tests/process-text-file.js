const fs = require("fs");
const path = require("path");
const os = require("os");

const regexp = new RegExp("6{3}|^[0-9]", "g");
const generateId = uuid => {
    if (regexp.test(uuid)) {
        uuid = generateId(uuid + 1);
    }

    return uuid;
};

const fileContents = fs.readFileSync(path.resolve(__dirname, "_input/1.15M-documents-orig.txt"), "utf8");
const fileLines = fileContents.split(os.EOL);
var result = "";

fileLines.forEach((fileLine, index) => {
    const word = fileLine.split("\t")[0];
    const uuid = generateId(index + 1000000);

    result += uuid + "\t" + word + "\n"; 
});

fs.writeFileSync(path.resolve(__dirname, "_input/1.15M-documents.txt"), result);

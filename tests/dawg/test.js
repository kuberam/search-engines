const Trie = require('dawg-lookup').Trie;
const PTrie = require('dawg-lookup/lib/ptrie').PTrie;
const fs = require("fs");

const replacements = {"ă": "a", "â": "a", "î": "i","ș": "s", "ş": "s","ț": "t","ţ": "t","á": "a","é": "e","í": "i","ó": "o","ú": "u","ắ": "a","ấ": "a","î́": "i", "à": "a"};
const replacementRegexp = new RegExp("[" + Object.keys(replacements).join("|") + "]", 'g');
const processResult = (result) => {
    return result.map(item => normalized_headwords_array[item]);
};
const makeid = () => {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 10; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

const rawDocuments = fs.readFileSync("../documents.json");
const documents = JSON.parse(rawDocuments);

const normalized_headwords_array = {};

console.time("words");
var wordsWithIndex = [];
var words = documents.map(
    (item, index) => {
        var s = item.s;
        s = s.toLowerCase();
        s = s.replace(replacementRegexp, m => replacements[m]);
        
        normalized_headwords_array[s] = [item.s, item.id];
        
        wordsWithIndex.push(s + "|" + index);
        
        return s;
    }    
);
words = [...new Set(words)];
words = words.sort();
fs.writeFileSync("./words.json", JSON.stringify(words));
fs.writeFileSync("./words-newlines.txt", words.join("\n"));
fs.writeFileSync("./words-newlines-index.txt", wordsWithIndex.join("\n"));
console.timeEnd("words");

var trie = new Trie(words.join(" "));
var packed = trie.pack();
fs.writeFileSync("./index.txt", packed);

var ptrie = new PTrie(packed);

console.log(ptrie.completions("p"));

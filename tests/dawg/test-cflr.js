const Trie = require('dawg-lookup').Trie;
const PTrie = require('dawg-lookup/lib/ptrie').PTrie;
const fs = require("fs");
const path = require("path");

let words = [];

const rawDocuments = fs.readFileSync("./tests/_input/c.txt", "utf-8");
rawDocuments.split(/\r?\n/).forEach(line =>  {
    words.push(line);
});
console.time("createIndex");
var trie = new Trie(words.join(" "));
var packed = trie.pack();
console.timeEnd("createIndex");
fs.writeFileSync(path.resolve(__dirname,  "./index/index.txt"), packed);

console.time("searchIndex");
var ptrie = new PTrie(packed);
console.log(ptrie.completions("cor"));
console.timeEnd("searchIndex");

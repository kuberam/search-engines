const { Index } = require("flexsearch");
const fs = require("fs");
const path = require("path");
const os = require("os");

const fileContents = fs.readFileSync(path.resolve(__dirname, "../_input/30k-documents.txt"), "utf8");
const fileLines = fileContents.split(os.EOL);

const index = new Index("memory", {
  encode: "balance",
  tokenize: "forward",
  threshold: 0
});
fileLines.forEach(fileLine => {
    const fileLineSplitted = fileLine.split("\t");
    const uuid = fileLineSplitted[0];
    const word = fileLineSplitted[1];

    index.add(uuid, word);
});

console.time("search1")
var result = index.search("ab");
console.timeEnd("search1")
console.log(result);

const searchIndexPath = path.resolve(__dirname, './index/30k-documents.json');
const exportIndex = () => {
  fs.writeFileSync(searchIndexPath, '');

  index.export((key, data) => {
	// create json objects with the key, data
    fs.appendFileSync(searchIndexPath, `{"${key}":${data === undefined ? "null" : data}},`);
  })
};
const importIndex = () => {
  const file = fs.readFileSync(searchIndexPath)
  // in order to parse it properly, an array has to be wrapped around the json
  return new Index(JSON.parse(`[${file.slice(0, -1)}]`));
}

exportIndex();

const importedIndex = importIndex();
console.log(importedIndex);
console.time("search2")
//result = importedIndex.search("ab");
console.timeEnd("search2")
console.log(result);
/* index.export(function(key, data){ 
    
  return new Promise(function(resolve){
    fs.writeFileSync(path.resolve(__dirname, "index/30k-documents.json"), JSON.stringify(data));      
    console.log(key);

    resolve();
  });
}); */

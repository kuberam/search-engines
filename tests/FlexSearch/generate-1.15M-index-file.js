const { Index } = require("flexsearch");
const fs = require("fs");
const path = require("path");
const os = require("os");

const fileContents = fs.readFileSync(path.resolve(__dirname, "../_input/c.txt"), "utf8");
const fileLines = fileContents.split(os.EOL);

const index = new Index("memory", {
  encode: "balance",
  tokenize: "forward",
  threshold: 0
});
fileLines.forEach((fileLine, i) => {
    //const fileLineSplitted = fileLine.split("\t");
    //const uuid = fileLineSplitted[0];
    //const word = fileLineSplitted[1];

    index.add(i, fileLine);
});

console.time("search1")
var result = index.search("corespunde");
console.timeEnd("search1")
console.log(result);

const searchIndexPath = path.resolve(__dirname, './index/1.15M-documents.json');
const exportIndex = () => {
  fs.writeFileSync(searchIndexPath, '');

  index.export((key, data) => {
	// create json objects with the key, data
    fs.appendFileSync(searchIndexPath, `{"${key}":${data === undefined ? "null" : data}},`);
  })
};

exportIndex();
/* index.export(function(key, data){ 
    
  return new Promise(function(resolve){
    fs.writeFileSync(path.resolve(__dirname, "index/30k-documents.json"), JSON.stringify(data));      
    console.log(key);

    resolve();
  });
}); */

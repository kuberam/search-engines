const MiniSearch = require("./minisearch.min.js");
const fs = require("fs");
const path = require("path");
const os = require("os");

const fileContents = fs.readFileSync(path.resolve(__dirname, "../_input/30k-documents.txt"), "utf8");
const fileLines = fileContents.split(os.EOL);
const processedDocuments = fileLines.map(fileLine => {
    const fileLineSplitted = fileLine.split("\t");
    const uuid = fileLineSplitted[0];
    const word = fileLineSplitted[1];

    return {"id": uuid, "s": word}
});

const searcheableFields = ["s"];
const storedFields = ["s"];
const miniSearch = new MiniSearch({
    fields: searcheableFields,
    storeFields: storedFields
});

miniSearch.addAll(processedDocuments); 

console.time("search1")
var result = miniSearch.search("ab", {prefix: true, fields: ["s"]});
console.timeEnd("search1")

console.log(result);

const rawIndex = JSON.stringify(miniSearch);
const indexModule = `
const index = '${rawIndex}';
const searcheableFields = ${JSON.stringify(searcheableFields)};
const storedFields = ${JSON.stringify(storedFields)};

export { index, searcheableFields, storedFields };
`;
fs.writeFileSync(path.resolve(__dirname, "index/30k-documents.json"), indexModule);
const fs = require("fs");

const rawDocuments = fs.readFileSync("./documents.json");
const documents = JSON.parse(rawDocuments);
var word_lengths = {};

console.time("word_lengths");
const words = documents
    .map(item => item.s.split(",").map(word => word.trim()).filter(word => !word.startsWith("-")))
    .flat()
    ;
console.log(JSON.stringify(words));
const max_word_length = Math.max(...words.map(word => word.length));
for (var i = 0; i < max_word_length; i++) {
    word_lengths[i + 1] = 0;
}
words.forEach(word => {++word_lengths[word.length]; if (word.length === 18) {console.log(word);}});

console.log(word_lengths);
//fs.writeFileSync("./words.json", JSON.stringify(words));
console.timeEnd("word_lengths");
/*
28
acăríță, acárniște, acárniță
26
pluguléț, plugușór, plugúț
porculéț, porcușór, porcúț
adevărăciúne, adevericiúne
24
albinícă, -nioáră, -níță
valideá, valideá-sultánă
23
bagrín, braghín, magrín
bădícă, bădíe, bădilúcă
bădișór, bădíță, bădiúc
22
adâncătúră, adâncitúră
cotorobátură, -robúșcă
amănunțíme, amărunțíme
oac!, oacacá!, oatatá!
aléi! aleléi! aleléle!
andreá, îndreá, undreá
21
acrișór, acríu, acrúț
Státu-pálmă-bárbă-cót
depărciór, depărtișór
nescái, -caivá, -cáre
Brustureánca, -reáscă
altmínteri, -mínterea
acúșa, acúși, acușíca
confiscațiúne, -cáție
confirmațiúne, -máție
asupravení, suprăvení
20
gógoriță, gogoroánță
conjugațiúne, -gáție
tulichímnă, tulipínă
albăstríu, albăstrúi
răzeșíță, răzeșoáică
bogheáscă, boghecúță
pochílnic, pochívnic
zdrámbițe, zdrambéle
pistruiát, pistruiéț
adăosătúră, adăosúră
bùnăvóie, bùnăvoínță
19
andrișá, -șél, -șíe
gheorghín, gherghín
constituționalitáte
părticea, părticică
gologóț, -móț, -móz
evghenís, evghenist
băietán, băiețándru
pânzișoáră, pânzúcă
vătășíță, vătășoáie
razăm, rázem, rázim
áide, áidem, áideți
malachián, malachíu
18
declináție, -țiúne
astrăiná, astriiná
achindéi, achindíe
prostuléț, prostúț
áltcevà, áltcinevà
chelepír, chelipír
usturoáie, -roiásă
bârdăhán, burduhán
amănunțí, amărunțí
trénchea-flénchea!
adevărát, adeverít
tractá, tractarisí
stiregíe, stirigíe
dezmétec, dezmétic
17
ací, acía, acíași
grofíță, grofoáie
éna mu che éna su
pătuceán, pătucél
mărgărintár, -rít
zdrang!, zdránga!
păticeán, pătceán
de-a înpicioárele
contractibilitáte
capigilàr-chehaiá
alócuri, alócurea
contemporaneitáte
dúcă-se-pe-pustíi
egumeneásă, -níță
alături, alăturea
*/

use fst::automaton;
use fst::{Automaton, IntoStreamer, Streamer};
use regex_automata::dense;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct Searcher {
    data: Vec<u8>,
}

#[wasm_bindgen]
impl Searcher {
    #[wasm_bindgen(constructor)]
    pub fn new(data: Vec<u8>) -> Searcher {
        Searcher { data }
    }

    pub fn prefix_search(&mut self, query: &str) -> Result<js_sys::Array, JsError> {
        let map = fst::Map::new(&self.data)?;

        let prefix = automaton::Str::new(query).starts_with();

        let mut stream = map.search(prefix).into_stream();

        let results = js_sys::Array::new();
        while let Some((_k, v)) = stream.next() {
            results.push(&wasm_bindgen::JsValue::from(v));
        }

        Ok(results)
    }

    pub fn regex_search(&mut self, query: &str) -> Result<js_sys::Array, JsError> {
        let map = fst::Map::new(&self.data)?;

        let formatted = format!(r"{}", query);
        let dfa = dense::Builder::new()
            .anchored(true)
            .build(formatted.as_str())
            .unwrap();

        let mut stream = map.search(&dfa).into_stream();

        let results = js_sys::Array::new();
        while let Some((_k, v)) = stream.next() {
            results.push(&wasm_bindgen::JsValue::from(v.to_string()));
        }

        Ok(results)
    }

    pub fn levenstein_1_search(&mut self, query: &str) -> Result<js_sys::Array, JsError> {
        let map = fst::Map::new(&self.data)?;

        let levenstein = automaton::Levenshtein::new(query, 1)?;

        let mut stream = map.search(levenstein).into_stream();

        let results = js_sys::Array::new();
        while let Some((_k, v)) = stream.next() {
            results.push(&wasm_bindgen::JsValue::from(v.to_string()));
        }

        Ok(results)
    }

    pub fn levenstein_2_search(&mut self, query: &str) -> Result<js_sys::Array, JsError> {
        let map = fst::Map::new(&self.data)?;

        let levenstein = automaton::Levenshtein::new(query, 2)?;

        let mut stream = map.search(levenstein).into_stream();

        let results = js_sys::Array::new();
        while let Some((_k, v)) = stream.next() {
            results.push(&wasm_bindgen::JsValue::from(v.to_string()));
        }

        Ok(results)
    }    
}

// wasm-pack build --release --target web
// wasm-pack build --release --target nodejs

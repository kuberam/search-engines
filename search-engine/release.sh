executable_name=${PWD##*/}
executable_name=${executable_name:-/}
executable_name=$(echo $executable_name | tr "-" "_")
echo "===$executable_name"
wasm-pack build --release --target web
rm -rf ./dist
mkdir -p ./dist
cp "./pkg/$executable_name.js" ./dist
cp "./pkg/"$executable_name"_bg.wasm" ./dist

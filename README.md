# Search engines

## Description

This repository contains search engines written in Rust and compiled to WebAssembly, in order to be used in browser, to provide search functionality for static websites.

## Resources

* Regular Expression Matching with a Trigram Index, https://swtch.com/~rsc/regexp/regexp4.html

* Shrinking .wasm Size - Rust and WebAssembly, https://rustwasm.github.io/docs/book/reference/code-size.html

* WasmTree: Web Assembly for the Semantic Web, https://hal.science/hal-03262817/file/WASMTree__Web_Assembly_for_the_Semantic_Web.pdf

* 
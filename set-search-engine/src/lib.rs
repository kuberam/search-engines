extern crate stats_alloc;
extern crate wasm_bindgen;

use porigon::levenshtein::LevenshteinAutomatonBuilder;
use porigon::{SearchableStorage, TopScoreCollector};
use rkyv::{
    archived_root,
    ser::{serializers::AllocSerializer, Serializer},
    Archive, Serialize,
};
use stats_alloc::{StatsAlloc, INSTRUMENTED_SYSTEM};
use std::alloc::System;
use wasm_bindgen::prelude::*;

#[global_allocator]
static GLOBAL: &StatsAlloc<System> = &INSTRUMENTED_SYSTEM;

#[derive(serde::Serialize)]
pub struct MemoryStats {
    pub allocations: usize,
    pub deallocations: usize,
    pub bytes_used: isize,
}

#[wasm_bindgen]
pub fn memory_stats() -> JsValue {
    let stats = GLOBAL.stats();
    let result = MemoryStats {
        allocations: stats.allocations + stats.reallocations,
        deallocations: stats.deallocations,
        bytes_used: stats.bytes_allocated as isize - stats.bytes_deallocated as isize,
    };

    JsValue::from_serde(&result).unwrap()
}

#[derive(Archive, Serialize)]
struct SearchData {
    headwords: SearchableStorage,
}

impl ArchivedSearchData {
    pub fn from_bytes(bytes: &Vec<u8>) -> &Self {
        unsafe { archived_root::<SearchData>(bytes.as_slice()) }
    }
}

impl SearchData {
    pub fn to_bytes(&self) -> Result<Vec<u8>, JsError> {
        let mut serializer = AllocSerializer::<4096>::default();
        serializer.serialize_value(self)?;
        Ok(serializer.into_serializer().into_inner().into_vec())
    }
}

#[wasm_bindgen]
pub struct Searcher {
    data: Vec<u8>,
    collector: TopScoreCollector,
    levenshtein_builder_1: LevenshteinAutomatonBuilder,
    levenshtein_builder_2: LevenshteinAutomatonBuilder,
}

#[wasm_bindgen]
impl Searcher {
    #[wasm_bindgen(constructor)]
    pub fn new(data: Vec<u8>, limit: usize) -> Searcher {
        let collector = TopScoreCollector::new(limit);
        let levenshtein_builder_1 = LevenshteinAutomatonBuilder::new(1, false);
        let levenshtein_builder_2 = LevenshteinAutomatonBuilder::new(2, false);
        Searcher {
            data,
            collector,
            levenshtein_builder_1,
            levenshtein_builder_2,
        }
    }

    pub fn search(&mut self, query: &str) -> JsValue {
        let data = ArchivedSearchData::from_bytes(&self.data);

        let headwords = data.headwords.to_searchable().unwrap();

        self.collector.reset();

        self.collector.consume_stream(
            headwords
                .exact_match(query)
        );
        self.collector.consume_stream(
            headwords
                .starts_with(query)
        );

        let results: Vec<u64> = self
            .collector
            .top_documents()
            .iter()
            .map(|doc| doc.index)
            .collect();
        JsValue::from_serde(&results).unwrap()
    }

    pub fn regexp(&mut self, query: &str) -> JsValue {
        let data = ArchivedSearchData::from_bytes(&self.data);

        let headwords = data.headwords.to_searchable().unwrap();

        self.collector.reset();

        self.collector.consume_stream(
            headwords
                .regexp(query)
        );

        let results: Vec<u64> = self
            .collector
            .top_documents()
            .iter()
            .map(|doc| doc.index)
            .collect();

        JsValue::from_serde(&results).unwrap()
    }  
    
    pub fn levenstein_1(&mut self, query: &str) -> JsValue {
        let data = ArchivedSearchData::from_bytes(&self.data);

        let headwords = data.headwords.to_searchable().unwrap();

        self.collector.reset();

        if query.len() > 3 {
            // running the levenshtein matchers on short query strings is quite expensive, so don't do that
            self.collector.consume_stream(
                headwords
                    .levenshtein_exact_match(&self.levenshtein_builder_1, query)
            );
            self.collector.consume_stream(
                headwords
                    .levenshtein_starts_with(&self.levenshtein_builder_1, query)
            );
        }

        let results: Vec<u64> = self
            .collector
            .top_documents()
            .iter()
            .map(|doc| doc.index)
            .collect();

        JsValue::from_serde(&results).unwrap()
    }
    
    pub fn levenstein_2(&mut self, query: &str) -> JsValue {
        let data = ArchivedSearchData::from_bytes(&self.data);

        let headwords = data.headwords.to_searchable().unwrap();

        self.collector.reset();

        if query.len() > 3 {
            // running the levenshtein matchers on short query strings is quite expensive, so don't do that
            self.collector.consume_stream(
                headwords
                    .levenshtein_exact_match(&self.levenshtein_builder_2, query)
            );
            self.collector.consume_stream(
                headwords
                    .levenshtein_starts_with(&self.levenshtein_builder_2, query)
            );
        }

        let results: Vec<u64> = self
            .collector
            .top_documents()
            .iter()
            .map(|doc| doc.index)
            .collect();

        JsValue::from_serde(&results).unwrap()
    }    
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct BuildData {
    headwords: Vec<(u64, String)>,
}

#[wasm_bindgen]
pub fn build(val: &JsValue) -> Result<Vec<u8>, JsError> {
    let data: BuildData = val.into_serde()?;
    let build_searchable = |input: Vec<(u64, String)>| {
        let mut i: Vec<(&str, u64)> = input
            .iter()
            .map(|(key, val)| (val.as_str(), *key))
            .collect();
        i.sort_by_key(|(key, _)| *key);
        SearchableStorage::build_from_iter(i)
    };
    let search_data = SearchData {
        headwords: build_searchable(data.headwords)?,
    };
    Ok(search_data.to_bytes()?)
}

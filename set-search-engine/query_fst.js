const fs = require('fs');
const readline = require('readline');
const { Searcher, memory_stats } = require('./pkg/set_search_engine');

function transliterate(s) {
    // https://towardsdatascience.com/difference-between-nfd-nfc-nfkd-and-nfkc-explained-with-python-code-e2631f96ae6c
    // https://en.wikipedia.org/wiki/Combining_character#Unicode_ranges
    return s.normalize("NFKD").replace(/[\u0300-\u036F\u1AB0-\u1AFF\u1DC0-\u1DFF\u20D0-\u20FF\uFE20-\uFE2F]/g, "").toLowerCase();
}

function askUserForQuery(rl) {
    return new Promise((resolve) => {
        rl.question('Search query: ', resolve);
    })
}

async function main() {
    console.time('Reading lookup');
    const wordRecords = JSON.parse(fs.readFileSync('./data/word-records.json', 'utf8'));
    console.timeEnd('Reading lookup');
    console.time('Reading FST');
    const fst_data = fs.readFileSync('./data/fst.bin', null);
    console.timeEnd('Reading FST');
    console.time('Initing searcher');
    const searcher = new Searcher(fst_data, 100);
    console.timeEnd('Initing searcher');
    const memUsageInMb = Math.round(process.memoryUsage().heapUsed / 1024 / 1024);
    console.log(`Process memory usage: ${memUsageInMb}MB`);

    let query = "ab";

    console.time('Querying FST took');
    let results = searcher.search(query);
    //console.log(results);
    console.timeEnd('Querying FST took');
    //displayResults(results, wordRecords);    
    //console.log();

/*     console.time('Regexp querying FST took');
    results = searcher.regexp(".*RE");
    console.log(results);
    console.timeEnd('Regexp querying FST took');
    displayResults(results, wordRecords);
    console.log();
    
    console.time('Levenstein 1 querying FST took');
    results = searcher.levenstein_1("daca");
    console.log(results);
    console.timeEnd('Levenstein 1 querying FST took'); 
    displayResults(results, wordRecords);    
    console.log();
    
    console.time('Levenstein 2 querying FST took');
    results = searcher.levenstein_2("daca");
    console.log(results);
    console.timeEnd('Levenstein 2 querying FST took'); 
    displayResults(results, wordRecords);    
    console.log(); */

    const stats = memory_stats();
    const bytesUsedInMb = Math.round(stats.bytes_used / 1024 / 1024 * 100) / 100;
    //console.log(`\nWASM memory usage: ${bytesUsedInMb}MB`);
}

main()
    .catch(err => {
        console.error(err);
        process.exit(1);
    });

function displayResults(results, wordRecords) {
    for (const index of results) {
        let wordRecord = wordRecords[index];
        let wordRecordID = getWordRecordID(index);

        console.log(`${wordRecord}|${wordRecordID}`);
    }
}

function getWordRecordID(index) {
    return (index + "").padStart(7, "0");
}


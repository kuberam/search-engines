const fs = require('fs');
const readline = require('readline');
const wasmExample = require('./pkg/set_search_engine');

function openFile(filename) {
  return readline.createInterface({
    input: fs.createReadStream(filename),
    crlfDelay: Infinity,
  });
}

const noAccentsReplacements = {"ä": "a", "á": "a","à": "a", "é": "e","í": "i","ì": "i", "ó": "o","ò":"o","ú": "u","ắ": "ă","ằ":"ă","ấ": "â","ầ": "â", "î": "î", "è": "e", "ù": "u", "ý": "y", "\u0301": "", "ń": "n", "ñ": "n", "ű": "u"};
const noAccentsReplacementsRegexp = new RegExp("[" + Object.keys(noAccentsReplacements).join("|") + "]", 'g');
const transliterate = (s) => {
    // https://towardsdatascience.com/difference-between-nfd-nfc-nfkd-and-nfkc-explained-with-python-code-e2631f96ae6c
    // https://en.wikipedia.org/wiki/Combining_character#Unicode_ranges
    //return s.normalize("NFKD").replace(/[\u0300-\u036F\u1AB0-\u1AFF\u1DC0-\u1DFF\u20D0-\u20FF\uFE20-\uFE2F]/g, "").toLowerCase();

    return s.toLowerCase().replace(noAccentsReplacementsRegexp, m => noAccentsReplacements[m]);
}

async function readFromPositionalIndex() {
    const strm = openFile('./data/documents.tsv');
    const headwords = [], wordRecords = [], lettersAndFrequencies = {};

    let index = 0;
    for await (const line of strm) {
        let [ positions, headword, gramGrp ] = line.split("\t");
        let normalisedHeadword = transliterate(headword);

        // get the letters
        let letters = headword.split("");

        letters.forEach(letter => {
            letter = letter.toLowerCase();
            if ("  1234567890-,!́".includes(letter)) {
                return;
            }            
            if (lettersAndFrequencies.hasOwnProperty(letter)) {
                ++lettersAndFrequencies[letter];
            } else {
                lettersAndFrequencies[letter] = 1;
            }
        });

        headwords.push([index, normalisedHeadword]);
        console.log(positions);
        let fullHeadword = `${headword} ${gramGrp}`.trim();
        wordRecords.push(`${fullHeadword}|${positions}`);

        index++;
    }
    
    let lettersAndFrequenciesMap = Object.entries(lettersAndFrequencies)
        .map(([key, value]) => `${key}\t${value}`);

    lettersAndFrequenciesMap.sort(new Intl.Collator('ro').compare);
    fs.writeFileSync("./data/letters.tsv", lettersAndFrequenciesMap.join("\n"), 'utf8');

    return {
        "headwords": headwords,
        "wordRecords": wordRecords
    };
}

async function buildFst(headwords) {
    return wasmExample.build({ headwords });
}

async function writeJSONtoDisk(data, filename) {
    fs.writeFileSync(`./data/${filename}.json`, JSON.stringify(data), 'utf8');
}

async function writeFstToDisk(fst) {
    fs.writeFileSync('./data/fst.bin', fst);
}

async function timeIt(label, fn) {
    console.log(label + '...');
    console.time(label + ' took');
    const ret = await fn();
    console.timeEnd(label + ' took');
    const memUsageInMb = Math.round(process.memoryUsage().heapUsed / 1024 / 1024);
    console.log(`Current memory usage: ${memUsageInMb} MB`);

    return ret;
}

async function main() {
    const { headwords, wordRecords } = await timeIt('Reading from word records dataset', readFromPositionalIndex);
    const fst = await timeIt('Building FST', () => buildFst(headwords));
    await timeIt('Writing FST to disk', () => writeFstToDisk(fst));
    await timeIt('Writing word records to disk', () => writeJSONtoDisk(wordRecords, "word-records"));
}

main()
    .catch(err => {
        console.error(err);
        process.exit(1);
});
// ./vellum set headwords.csv headwords-set.fst --sorted
// ./vellum map /home/claudius/workspace/repositories/git/github.com/mcuelenaere/porigon/wasm-example/data-clre/headwords.csv headwords-map.fst --sorted
// ./nfsa_build -i headwords.csv -o headwords.fst
// wasm-pack build --target web
// wasm-pack build --target nodejs

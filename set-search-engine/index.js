import init, {Searcher, memory_stats} from "./pkg/search_engine.js";

await init();

console.time('Reading lookup');
let lookup = await fetch("./data/word-records.json").then(response => response.json());
console.timeEnd('Reading lookup');

console.time('Reading FST');
let fst_data = await fetch("./data/fst.bin").then(response => response.arrayBuffer());
fst_data  = new Uint8Array(fst_data);
console.timeEnd('Reading FST');

console.time('Initing searcher1');
const searcher = new Searcher(fst_data, 1000000);
console.timeEnd('Initing searcher1');

let queryString = "aburit";

console.time('Querying FST took');
const results = searcher.search(queryString);
console.log(results);
console.timeEnd('Querying FST took');

const stats = memory_stats();
const bytesUsedInMb = Math.round(stats.bytes_used / 1024 / 1024 * 100) / 100;
console.log(`WASM memory usage: ${bytesUsedInMb}MB`);

console.time('Displaying results took');
results.forEach(item => {
  const [ fullHeadword, positions ] = lookup[parseInt(item.index)].split("|");
  console.log(`${fullHeadword} | https://clre.solirom.ro/${positions}`);
});
console.timeEnd('Displaying results took');
